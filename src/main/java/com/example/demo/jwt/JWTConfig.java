package com.example.demo.jwt;

import javax.crypto.SecretKey;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.http.HttpHeaders;

import io.jsonwebtoken.security.Keys;

@ConfigurationProperties(prefix = "application.jwt")
public class JWTConfig {

	private String secretKey;
	private String tokenPrefix;
	private String tokenExpirationDays;
	
	public JWTConfig() {
	}

	public String getSecretKey() {
		return secretKey;
	}

	public void setSecretKey(String secretKey) {
		this.secretKey = secretKey;
	}

	public String getTokenPrefix() {
		return tokenPrefix;
	}

	public void setTokenPrefix(String tokenPrefix) {
		this.tokenPrefix = tokenPrefix;
	}

	public String getTokenExpirationDays() {
		return tokenExpirationDays;
	}

	public void setTokenExpirationDays(String tokenExpirationDays) {
		this.tokenExpirationDays = tokenExpirationDays;
	}	
	
	@Bean
	public SecretKey getSecretKeyForSigning() {
		return Keys.hmacShaKeyFor(secretKey.getBytes());
	}
	
	public String getAuthorization() {
		return HttpHeaders.AUTHORIZATION;
	}
}
