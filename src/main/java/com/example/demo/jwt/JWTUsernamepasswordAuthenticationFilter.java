package com.example.demo.jwt;

import java.time.LocalDate;
import java.util.Date;

import javax.crypto.SecretKey;
import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.io.IOException;
import io.jsonwebtoken.security.Keys;

public class JWTUsernamepasswordAuthenticationFilter extends UsernamePasswordAuthenticationFilter {
	
	private AuthenticationManager authenticationManager;
	private JWTConfig jwtConfig;
	private SecretKey secretKey;
	
	
	public JWTUsernamepasswordAuthenticationFilter(AuthenticationManager authenticationManager,JWTConfig jwtConfig,SecretKey secretKey){
		this.jwtConfig=jwtConfig;
		this.authenticationManager=authenticationManager;
		this.secretKey=secretKey;
	}
	
	@Override
	public Authentication attemptAuthentication(HttpServletRequest request, HttpServletResponse response)
			throws AuthenticationException {
		try {
		UsernamePasswordRequest authenticationReq=new ObjectMapper()
				.readValue(request.getInputStream(), UsernamePasswordRequest.class);
		
		Authentication authentication=new UsernamePasswordAuthenticationToken(authenticationReq.getUsername(), 
				authenticationReq.getPassword());
		
		Authentication authenticate= authenticationManager.authenticate(authentication);
		return authenticate;
		
		}catch (IOException e) {
			throw new RuntimeException(e);
		} catch (JsonParseException e) {
			throw new RuntimeException(e);
		} catch (JsonMappingException e) {
			throw new RuntimeException(e);
		} catch (java.io.IOException e) {
			throw new RuntimeException(e);
		}
	}
	
	
	@Override
	protected void successfulAuthentication(HttpServletRequest request, HttpServletResponse response, FilterChain chain,
			Authentication authResult) throws java.io.IOException, ServletException {
		String token=Jwts.builder()
		.setSubject(authResult.getName())
		.claim("authorities", authResult.getAuthorities())
		.setIssuedAt(new Date())
		.setExpiration(java.sql.Date.valueOf(LocalDate.now().plusWeeks(2)))
		.signWith(secretKey)
		.compact();
		
		response.addHeader(jwtConfig.getAuthorization(),jwtConfig.getTokenPrefix()+token);
	}

}
