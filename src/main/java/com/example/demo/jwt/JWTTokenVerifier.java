package com.example.demo.jwt;

import java.io.IOException;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

import javax.crypto.SecretKey;
import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.filter.OncePerRequestFilter;

import com.google.common.base.Strings;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jws;
import io.jsonwebtoken.JwtException;
import io.jsonwebtoken.Jwts;

public class JWTTokenVerifier extends OncePerRequestFilter {

	@Autowired
	private JWTConfig jwtConfig;
	@Autowired
	private SecretKey secretKey;
	
	@Override
	protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain)
			throws ServletException, IOException {		
		String authorization=request.getHeader(jwtConfig.getAuthorization());		
		if(Strings.isNullOrEmpty(authorization)|| !authorization.startsWith(jwtConfig.getTokenPrefix())) {
			filterChain.doFilter(request, response);
			return;
		}
		String token=authorization.replace(jwtConfig.getTokenPrefix(),"");
		
		
		try {
			Jws<Claims> claims=Jwts.parser().setSigningKey(secretKey) .parseClaimsJws(token);
              Claims body=claims.getBody();
				String username=body.getSubject();
				List<Map<String,String>> authorities=(List<Map<String,String>>)body.get("authorities");
				
				Set<SimpleGrantedAuthority> simpleGrantedAuthority =authorities.stream().map(m->new SimpleGrantedAuthority(m.get("authority"))).collect(Collectors.toSet());
				
				Authentication authentication=new UsernamePasswordAuthenticationToken(username,null,simpleGrantedAuthority);
				SecurityContextHolder.getContext().setAuthentication(authentication);
			
		}catch (JwtException e) {
			throw new IllegalStateException(String.format("Token %s is not valid", token));
		}
		
		filterChain.doFilter(request, response);		
		
	}

}
