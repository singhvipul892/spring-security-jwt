package com.example.demo.security;


import javax.crypto.SecretKey;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.dao.DaoAuthenticationProvider;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.password.PasswordEncoder;

import com.example.demo.jwt.JWTConfig;
import com.example.demo.jwt.JWTTokenVerifier;
import com.example.demo.jwt.JWTUsernamepasswordAuthenticationFilter;

@Configuration
@EnableWebSecurity 
@EnableGlobalMethodSecurity(prePostEnabled = true,securedEnabled = true)
public class ApplicationSecurityConfig extends WebSecurityConfigurerAdapter {

	public final PasswordEncoder passwordEncoder;
	private final UserDetailsService applicationUserService;
	private final JWTConfig jwtConfig;
	private final SecretKey secretKey;
	
	@Autowired
	public ApplicationSecurityConfig(PasswordEncoder passwordEncoder, UserDetailsService applicationUserService,
			JWTConfig jwtConfig,SecretKey secretKey) {
		super();
		this.passwordEncoder = passwordEncoder;
		this.applicationUserService = applicationUserService;
		this.jwtConfig=jwtConfig;
		this.secretKey=secretKey;
	}
	
	
	@Override    
    protected void configure(HttpSecurity http) throws Exception {
    	http
    	.csrf().disable()
    	.sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS)
    	.and()
    	.addFilter(new JWTUsernamepasswordAuthenticationFilter(authenticationManager(),jwtConfig,secretKey))
    	.addFilterAfter(new JWTTokenVerifier(),JWTUsernamepasswordAuthenticationFilter.class)
    	.authorizeRequests()
    	.antMatchers("/login","/index","css/*","js/*").permitAll()
    	.antMatchers("/api/**").hasRole(ApplicationUserRole.STUDENT.name())
		.anyRequest()
    	.authenticated();
    }
	
	@Override
	protected void configure(AuthenticationManagerBuilder auth) throws Exception {
		auth.authenticationProvider(daoAuthenticationProvider());
	}

	@Bean
	public AuthenticationProvider daoAuthenticationProvider() {
		DaoAuthenticationProvider daoAuthenticationProvider=new DaoAuthenticationProvider();
		daoAuthenticationProvider.setPasswordEncoder(passwordEncoder);
		daoAuthenticationProvider.setUserDetailsService(applicationUserService);
		return daoAuthenticationProvider;
	}
	
}

/*
 * .antMatchers(HttpMethod.POST,"/management/api/**").hasAuthority(STUDENT_WRITE
 * .getPermission())
 * .antMatchers(HttpMethod.DELETE,"/management/api/**").hasAuthority(
 * STUDENT_WRITE.getPermission())
 * .antMatchers(HttpMethod.PUT,"/management/api/**").hasAuthority(STUDENT_WRITE.
 * getPermission())
 * .antMatchers(HttpMethod.GET,"/management/api/**").hasAnyRole(ADMIN.name(),
 * ADMINTRAINEE.name())
 */



/*
 * @Override
 * 
 * @Bean protected UserDetailsService userDetailsService() { UserDetails
 * vipulUSer=User.builder() .username("Vipul")
 * .password(passwordEncoder.encode("Ketty")) //
 * .roles(ApplicationUserRole.STUDENT.name())
 * .authorities(STUDENT.getGrantedAuthorties()) .build();
 * 
 * UserDetails hobbitUSer=User.builder() .username("Hobbit")
 * .password(passwordEncoder.encode("hobbit"))
 * .authorities(ADMIN.getGrantedAuthorties())
 * //.roles(ApplicationUserRole.ADMIN.name()) .build();
 * 
 * UserDetails gittiUSer=User.builder() .username("Gitti")
 * .password(passwordEncoder.encode("gitti"))
 * .authorities(ADMINTRAINEE.getGrantedAuthorties())
 * //.roles(ApplicationUserRole.ADMINTRAINEE.name()) .build();
 * 
 * return new InMemoryUserDetailsManager(vipulUSer,hobbitUSer,gittiUSer);
 * 
 * }
 */
